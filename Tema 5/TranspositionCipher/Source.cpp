#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

//Verifica daca toate caracterele din cheie sunt unice.
bool uniqueCharacters(std::string str)
{
	std::sort(str.begin(), str.end());

	for (int index = 0; index < str.length(); ++index)
		if (str[index] == str[index + 1])
			return false;

	return true;
}

//Verifica daca toate caracterele din cheie sunt majuscule.
bool isUpper(std::string str)
{
	for (char ch : str)
		if (islower(ch))
			return false;

	return true;
}

//Citirea unei chei valide de la utilizator.
std::string getKey()
{
	std::cout << "Enter key: ";
	std::string key;
	std::cin >> key;

	while (true)
	{
		if (!uniqueCharacters(key) || !isUpper(key))
		{
			std::cout << "Please enter a valid key: ";
			std::cin >> key;
		}
		else
			break;
	}

	return key;
}

//Setarea ordinii literelor din cheie in functie de ordinea lor in alfabet.
void setPermutationOrder(std::map<int, int>& keyMap, std::string& key)
{
	for (int index = 0; index < key.length(); ++index)
		keyMap[key[index]] = index;
}

//Encriptarea mesajului cu cifrul de transpozitie.
std::string encryptMessage(std::string message, std::string& key, std::map<int, int>& keyMap)
{
	//Setarea numarului de linii si coloane in matrice.
	int col = key.length();
	int row = message.length() / col;

	//Daca mesajul este mai lung decat dimensiunea prestabilita, se trece pe urmatorul rand.
	if (message.length() % col)
		++row;

	//Daca matricea nu este completata integral, se compleaza cu litere lowercase din alfabetul englezesc.
	int index = 0;
	while (message.length() % key.length())
	{
		std::string alphabet = "abcdefghijklmnopqrstuvwxyz";
		message.push_back(alphabet[index]);
		index++;
	}
	
	//Alocarea memoriei pentru matrice.
	std::vector<std::vector<char>> matrix(row);
	for (int index = 0; index < row; ++index)
		matrix[index].resize(col);

	//Umplerea matricii cu literele din mesaj.
	for (int i = 0, k = 0; i < row; ++i)
	{
		for (int j = 0; j < col; )
		{
			if (isalpha(message[k]) || message[k] == ' ')
			{
				matrix[i][j] = message[k];
				++j;
			}
			++k;
		}
	}

	//Criptarea mesajului prin parcurgerea matricii pe coloane, in functie de ordinea literelor din cheie.
	std::string cipher = "";
	int j;

	for (std::map<int, int>::iterator it = keyMap.begin(); it != keyMap.end(); ++it)
	{
		j = it->second;

		for (int i = 0; i < row; ++i)
			if (isalpha(matrix[i][j]) || matrix[i][j] == ' ')
				cipher += matrix[i][j];
	}

	return cipher;
}

int main()
{
	std::cout << "Enter message: ";
	std::string message;
	std::getline(std::cin, message);

	std::string key = getKey();

	std::map<int, int> keyMap;
	setPermutationOrder(keyMap, key);

	std::string cipher = encryptMessage(message, key, keyMap);

	std::cout << "Encrypted message: " << cipher << '\n';

	return 0;
}
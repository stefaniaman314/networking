#include<iostream>
#include<conio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>

#include "SelectiveRepeat.h"

using namespace std;

void SelectiveRepeat::input()
{

	//no. of bits for the frame
	int n;
	cout << "Enter the no. of bits for the sequence: ";
	cin >> n;

	//no. of frames from n bits
	int m = pow(2, n);    

	//maximum size of the window
	fr_send_at_instance = (m / 2);

	int t = 0;

	for (int i = 0; i < TOT_FRAMES; i++)
	{
		arr[i] = t;
		t = (t + 1) % m;
	}

	for (int i = 0; i < fr_send_at_instance; i++)
	{
		send[i] = arr[i];
		rcvd[i] = arr[i];
		rcvd_ack[i] = false;
	}

	//sender's windows = receiver's windows
	rw = sw = fr_send_at_instance;

	sender(m);
}

void SelectiveRepeat::sender(int m)
{
	for (int i = 0; i < fr_send_at_instance; i++)	
		if (rcvd_ack[i] == false)
			cout << "SENDER : Frame " << send[i] << " is sent\n";
	
	receiver(m);
}

void SelectiveRepeat::receiver(int m)
{
	time_t t;
	srand((unsigned)time(&t));

	for (int i = 0; i < fr_send_at_instance; i++)
	{
		if (rcvd_ack[i] == false)
		{
			int f = rand() % 10;

			//if f=5 frame is discarded for some reason
			if (f != 5)
			{
				for (int j = 0; j < fr_send_at_instance; j++)
					if (rcvd[j] == send[i])
					{
						cout << "RECIEVER : Frame " << rcvd[j] << " recieved correctly\n";
						rcvd[j] = arr[rw];
						rw = (rw + 1) % m;
						break;
					}

				if (fr_send_at_instance == 0)
					cout << "RECIEVER : Duplicate frame" << send[i] << " discarded\n";
;
				int a1 = rand() % 5;

				//if al==3 then ack is lost
				if (a1 == 3)
				{
					cout << "(acknowledgement " << send[i] << " lost)\n";
					cout << "(sender timeouts-->Resend the frame)\n";
					rcvd_ack[i] = false;

				}
				//else recieved
				else
				{
					cout << "(acknowledgement " << send[i] << " recieved)\n";
					rcvd_ack[i] = true;
				}

			}
			//else frame is correctly recieved
			else
			{
				int ld = rand() % 2;

				//if frame damaged
				if (ld == 0)
				{
					cout << "RECEIVER : Frame " << send[i] << " is damaged\n";
					cout << "RECEIVER : Negative Acknowledgement " << send[i] << " sent\n";

				}
				//else frame lost
				else
				{
					cout << "RECEIVER : Frame " << send[i] << " is lost\n";
					cout << "(SENDER TIMEOUTS-->RESEND THE FRAME)\n";
				}
				rcvd_ack[i] = false;
			}
		}
	}

	for (int j = 0; j < fr_send_at_instance; j++)
	{
		if (rcvd_ack[j] == false)
			break;
	}

	int i = 0;

	for (int k = 0; k < fr_send_at_instance; k++)
	{
		send[i] = send[k];
		if (rcvd_ack[k] == false)
			rcvd_ack[i] = false;
		else
			rcvd_ack[i] = true;
		i++;
	}

	if (i != fr_send_at_instance)
	{
		for (int k = i; k < fr_send_at_instance; k++)
		{
			send[k] = arr[sw];
			sw = (sw + 1) % m;
			rcvd_ack[k] = false;
		}
	}

	cout << "Want to continue?";
	char ch;
	cin >> ch;
	cout << "\n";
	if (ch == 'y')
		sender(m);
	else
		exit(0);
}

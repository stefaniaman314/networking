#pragma once

#define TOT_FRAMES 500
#define FRAMES_SEND 10

class SelectiveRepeat
{
public:
	void input();
	void sender(int);
	void receiver(int);

private:
	int fr_send_at_instance;
	int arr[TOT_FRAMES];
	int send[FRAMES_SEND];
	int rcvd[FRAMES_SEND];
	bool rcvd_ack[FRAMES_SEND];
	int sw;
	int rw;     
};


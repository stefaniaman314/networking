#include <iostream>
#include <math.h>

//Functie care returneaza valoarea lui a^b mod p
long long power(long long a, long long b, long long p)
{
	if (b == 1)
		return a;
	
	return (((long long)pow(a, b)) % p);
}

int main()
{
	//=== Chei publice ===

	//Selectarea numarului prim
	std::cout << "Enter a prime number: ";
	long long prime;
	std::cin >> prime;

	//Selectarea radacinii primitive a numarului prim
	std::cout << "Enter the primitive root: ";
	long long primitive;
	std::cin >> primitive;

	//=== Chei private ===

	//Selectarea cheii private A
	std::cout << "Enter private key A: ";
	long long privateKeyA;
	std::cin >> privateKeyA;
	//Generarea unei noi chei pe baza cheii private A
	long long generatedKeyA = power(primitive, privateKeyA, prime);

	//Selectarea cheii private B
	std::cout << "Enter private key B: ";
	long long privateKeyB;
	std::cin >> privateKeyB;
	//Generarea unei noi chei pe baza cheii private B
	long long generatedKeyB = power(primitive, privateKeyB, prime);

	//=== Chei secrete ===

	//Aflarea cheilor secrete dupa schimbul cheilor generate
	long long secretKeyA = power(generatedKeyB, privateKeyA, prime);
	long long secretKeyB = power(generatedKeyA, privateKeyB, prime);

	std::cout << "Secret key A: " << secretKeyA << '\n';
	std::cout << "Secret key B: " << secretKeyB << '\n';

	return 0;
}
import socket                
  
# create a socket object 
clientSocket = socket.socket()          
  
# define the port on which you want to connect 
port = 12345                
  
# connect to the server on local computer 
clientSocket.connect(('localhost', port)) 
  
# receive data from the server 
f = open ('Andrew_Solomon-Demonul_Amiezii.pdf', 'rb')
l = f.read(1024)

while (l):
    clientSocket.send(l)
    l = f.read(1024)

clientSocket.close()  
import socket                
  
# create a socket object 
serverSocket = socket.socket()          
print('Socket successfully created!')
  
# reserve a port
port = 12345                
  
# bind to the port 
serverSocket.bind(('localhost', port))         
print('Socket binded to port:', port) 
  
# put the socket into listening mode (maximum 5 connections)
serverSocket.listen(5)      
print('Socket is listening...')          
  
# loop until intrerupt or occuring an error
while True: 
  
   # establish connection with client
   connection, address = serverSocket.accept()      
   print('Got connection from', address)
   
   index = 1
   f = open('file_' + str(index) + '.pdf', 'wb') 
   index += 1

   while (True):       
      # receive data and write it to file
      l = serverSocket.recv(1024)

      while (l):
         f.write(l)
         l = serverSocket.recv(1024)

   f.close()

   serverSocket.close()

serverSocket.close()